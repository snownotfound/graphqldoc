package graphqldoc

import (
	"fmt"
	"sort"
)

// Query structure for query data
type Query struct {
	Name              string
	Description       string
	Args              []InputValue
	IsDeprecated      bool
	DeprecationReason string
	Type              *TypeRef
}

func createQueryFile() {
	var queries []Query
	for _, v := range schema.QueryType.Fields {
		if v.Name != "_" {
			queries = append(queries, Query{
				Name:              v.Name,
				Description:       v.Description,
				Args:              v.Args,
				IsDeprecated:      v.IsDeprecated,
				DeprecationReason: v.DeprecationReason,
				Type:              v.Type,
			})
		}
	}

	sort.Slice(queries[:], func(i, j int) bool {
		return queries[i].Name < queries[j].Name
	})

	parseFileTemplate("template/query/Home.md.tmpl", "query/Home.md", queries)
	for _, v := range queries {
		parseFileTemplate("template/query/query.md.tmpl", fmt.Sprintf("query/%s.md", v.Name), v)
	}
}

package graphqldoc

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strings"
	"text/template"
)

// TypeRef type in the schema.graphql
type TypeRef struct {
	Kind   string   `json:"kind"`
	Name   string   `json:"name"`
	OfType *TypeRef `json:"ofType"`
}

// InputValue type in the schema.graphql
type InputValue struct {
	Name         string      `json:"name"`
	Description  string      `json:"description"`
	DefaultValue interface{} `json:"defaultValue"`
	Type         *TypeRef    `json:"type"`
}

// Field type in the schema.graphql
type Field struct {
	Name              string       `json:"name"`
	Description       string       `json:"description"`
	Args              []InputValue `json:"args"`
	Type              *TypeRef     `json:"type"`
	IsDeprecated      bool         `json:"isDeprecated"`
	DeprecationReason string       `json:"deprecationReason"`
}

// EnumValues type in the schema.graphql
type EnumValues struct {
	Name              string `json:"name"`
	Description       string `json:"description"`
	IsDeprecated      bool   `json:"isDeprecated"`
	DeprecationReason string `json:"deprecationReason"`
}

// FullType type in the schema.graphql
type FullType struct {
	Kind          string       `json:"kind"`
	Name          string       `json:"name"`
	Description   string       `json:"description"`
	Fields        []Field      `json:"fields"`
	InputFields   []InputValue `json:"inputFields"`
	Interfaces    []TypeRef    `json:"interfaces"`
	EnumValues    []EnumValues `json:"enumValues"`
	PossibleTypes []TypeRef    `json:"possibleTypes"`
}

// TypeDirective type in the schema.graphql
type TypeDirective struct {
	Name        string       `json:"name"`
	Description string       `json:"description"`
	Args        []InputValue `json:"args"`
	OnOperation bool         `json:"onOperation"`
	OnFragment  bool         `json:"onFragment"`
	OnField     bool         `json:"onField"`
}

// Schema type in the schema.graphql
type Schema struct {
	QueryType        FullType        `json:"queryType"`
	MutationType     FullType        `json:"mutationType"`
	SubscriptionType FullType        `json:"subscriptionType"`
	Types            []FullType      `json:"types"`
	Directives       []TypeDirective `json:"directives"`
}

// Data schema graphql
type Data struct {
	Schema Schema `json:"__schema"`
}

// Response response server graphql
type Response struct {
	Data   Data                     `json:"data"`
	Errors []map[string]interface{} `json:"errors"`
}

func getSchemaGraphql(endpoint string) Schema {
	query, err := templateDIR.ReadFile("template/schema.graphql")
	checkError("Read schema.graphql from embedded filesystem", err)

	client := &http.Client{}
	req, err := http.NewRequest("GET", endpoint, nil)
	checkError("Connect with server graphql", err)

	q := req.URL.Query()
	q.Add("query", string(query))
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	checkError("Graphql response server", err)
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	checkError("Read response server graphql", err)

	var data Response
	json.Unmarshal(body, &data)
	if len(data.Errors) != 0 {
		checkError("Error response server graphql", fmt.Errorf("%v", data.Errors))
	}

	return data.Data.Schema
}

func getTypeLinkMarkdown(v *TypeRef) string {
	link := ""
	for v.OfType != nil {
		v = v.OfType
	}

	switch v.Kind {
	case "SCALAR":
		link = fmt.Sprintf("../scalar/%s%s", v.Name, extMd)
		break
	case "OBJECT":
		link = fmt.Sprintf("../object/%s%s", v.Name, extMd)
		break
	case "INPUT_OBJECT":
		link = fmt.Sprintf("../input/%s%s", v.Name, extMd)
		break
	case "ENUM":
		link = fmt.Sprintf("../enum/%s%s", v.Name, extMd)
		break
	}

	return link
}

func getTypeGraphql(v *TypeRef) string {
	name := "{{NAME}}"
	if v.OfType != nil {
		name = getTypeGraphql(v.OfType)
	}

	switch v.Kind {
	case "SCALAR", "OBJECT", "INPUT_OBJECT", "ENUM":
		name = strings.ReplaceAll(name, "{{NAME}}", v.Name)
		break
	case "NON_NULL":
		name = name + "!"
		break
	case "LIST":
		name = fmt.Sprintf("[%s]", name)
		break
	}

	return name
}

func parseFileTemplate(fileTemplate, file string, obj interface{}) {
	file = fmt.Sprintf("%s/%s", dir, file)
	if _, err := os.Stat(path.Dir(file)); os.IsNotExist(err) {
		os.MkdirAll(path.Dir(file), 0755)
	}
	// output files
	f, err := os.OpenFile(file, os.O_CREATE|os.O_WRONLY, 0644)
	checkError("parseFileTemplate - Create output file ["+file+"]", err)
	// input files from embedded template filesystem
	data, err := templateDIR.ReadFile(fileTemplate)
	checkError("parseFileTemplate - Read file ["+fileTemplate+"]", err)

	p, err := template.New("MD").Funcs(template.FuncMap{
		"getTypeGraphql":      getTypeGraphql,
		"getTypeLinkMarkdown": getTypeLinkMarkdown,
		"extLinkMarkdown":     func() string { return extMd },
	}).Parse(string(data))
	checkError("Generate template"+fileTemplate, err)

	t := template.Must(p, err)
	err = t.Execute(f, obj)
	checkError("Generate file "+fileTemplate, err)
}

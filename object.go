package graphqldoc

import (
	"fmt"
	"sort"
)

// Object structure for object data
type Object struct {
	Name        string
	Description string
	Fields      []Field
}

func createObjectFile() {
	var objects []Object
	for _, v := range schema.Types {
		if v.Kind == "OBJECT" && v.Name != "Query" && v.Name != "Mutation" {
			objects = append(objects, Object{
				Name:        v.Name,
				Description: v.Description,
				Fields:      v.Fields,
			})
		}
	}

	sort.Slice(objects[:], func(i, j int) bool {
		return objects[i].Name < objects[j].Name
	})

	parseFileTemplate("template/object/Home.md.tmpl", "object/Home.md", objects)
	for _, v := range objects {
		parseFileTemplate("template/object/object.md.tmpl", fmt.Sprintf("object/%s.md", v.Name), v)
	}
}

package graphqldoc

import (
	"embed"
	"log"
	"os"
)

var schema Schema
var dir string
var extMd string

// Embed the entire template directory.
//go:embed template/*
var templateDIR embed.FS

// Generate generate the files with the graphql documentation
func Generate(endpoint, outputDir string, withoutExt bool) {
	dir = outputDir
	extMd = ".md"
	if withoutExt {
		extMd = ""
	}

	if _, err := os.Stat(outputDir); !os.IsNotExist(err) {
		os.RemoveAll(outputDir + "/")
	}

	os.MkdirAll(outputDir, 0755)
	schema = getSchemaGraphql(endpoint)
	parseFileTemplate("template/Home.md.tmpl", "Home.md", nil)
	createScalarFile()
	createEnumFile()
	createInputFile()
	createObjectFile()
	createQueryFile()
	createMutationFile()
}

func checkError(tag string, err error) {
	if err != nil {
		log.Printf("Error: %s\n%v", tag, err)
		os.Exit(1)
	}
}

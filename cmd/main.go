package main

import (
	"flag"
	"log"
	"os"

	"gitlab.com/mvochoa/graphqldoc"
)

func main() {
	var outputDir string
	var endpoint string
	var withoutExt bool
	flag.StringVar(&endpoint, "endpoint", "", "Endpoint server graphql. Example https://dominio.com/graphql")
	flag.StringVar(&outputDir, "output-dir", "doc/", "Directory with documentation files")
	flag.BoolVar(&withoutExt, "without-ext", false, "Does not add extension to markdown file links (.md)")
	flag.Parse()

	if endpoint == "" {
		log.Println("Error: Endpoint required")
		os.Exit(1)
	}
	graphqldoc.Generate(endpoint, outputDir, withoutExt)
}

package graphqldoc

import (
	"fmt"
	"sort"
)

// Input structure for input data
type Input struct {
	Name        string
	Description string
	InputFields []InputValue
}

func createInputFile() {
	var inputs []Input
	for _, v := range schema.Types {
		if v.Kind == "INPUT_OBJECT" {
			inputs = append(inputs, Input{
				Name:        v.Name,
				Description: v.Description,
				InputFields: v.InputFields,
			})
		}
	}

	sort.Slice(inputs[:], func(i, j int) bool {
		return inputs[i].Name < inputs[j].Name
	})

	parseFileTemplate("template/input/Home.md.tmpl", "input/Home.md", inputs)
	for _, v := range inputs {
		parseFileTemplate("template/input/input.md.tmpl", fmt.Sprintf("input/%s.md", v.Name), v)
	}
}

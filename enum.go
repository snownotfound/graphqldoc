package graphqldoc

import (
	"fmt"
	"sort"
)

// Enum structure for enum data
type Enum struct {
	Name        string
	Description string
	Enums       []EnumValues
}

func createEnumFile() {
	var enums []Enum
	for _, v := range schema.Types {
		if v.Kind == "ENUM" {
			enums = append(enums, Enum{
				Name:        v.Name,
				Description: v.Description,
				Enums:       v.EnumValues,
			})
		}
	}

	sort.Slice(enums[:], func(i, j int) bool {
		return enums[i].Name < enums[j].Name
	})

	for _, v := range enums {
		sort.Slice(v.Enums[:], func(i, j int) bool {
			return v.Enums[i].Name < v.Enums[j].Name
		})
	}

	parseFileTemplate("template/enum/Home.md.tmpl", "enum/Home.md", enums)
	for _, v := range enums {
		parseFileTemplate("template/enum/enum.md.tmpl", fmt.Sprintf("enum/%s.md", v.Name), v)
	}
}

package graphqldoc

import (
	"fmt"
	"sort"
)

// Scalar structure for scalar data
type Scalar struct {
	Name        string
	Description string
}

func createScalarFile() {
	var scalars []Scalar
	for _, v := range schema.Types {
		if v.Kind == "SCALAR" {
			scalars = append(scalars, Scalar{
				Name:        v.Name,
				Description: v.Description,
			})
		}
	}

	sort.Slice(scalars[:], func(i, j int) bool {
		return scalars[i].Name < scalars[j].Name
	})

	parseFileTemplate("template/scalar/Home.md.tmpl", "scalar/Home.md", scalars)
	for _, v := range scalars {
		parseFileTemplate("template/scalar/scalar.md.tmpl", fmt.Sprintf("scalar/%s.md", v.Name), v)
	}
}

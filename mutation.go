package graphqldoc

import (
	"fmt"
	"sort"
)

// Mutation structure for mutation data
type Mutation struct {
	Name              string
	Description       string
	Args              []InputValue
	IsDeprecated      bool
	DeprecationReason string
	Type              *TypeRef
}

func createMutationFile() {
	var mutations []Mutation
	for _, v := range schema.MutationType.Fields {
		if v.Name != "_" {
			mutations = append(mutations, Mutation{
				Name:              v.Name,
				Description:       v.Description,
				Args:              v.Args,
				IsDeprecated:      v.IsDeprecated,
				DeprecationReason: v.DeprecationReason,
				Type:              v.Type,
			})
		}
	}

	sort.Slice(mutations[:], func(i, j int) bool {
		return mutations[i].Name < mutations[j].Name
	})

	parseFileTemplate("template/mutation/Home.md.tmpl", "mutation/Home.md", mutations)
	for _, v := range mutations {
		parseFileTemplate("template/mutation/mutation.md.tmpl", fmt.Sprintf("mutation/%s.md", v.Name), v)
	}
}

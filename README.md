# Documentation generator for GraphQL

Markdown generator for documenting GraphQL schema

Download binary in [Releases](https://gitlab.com/mvochoa/graphqldoc/-/releases)

## Use

Generate dir `doc/` with markdown files

```bash
$ graphqldoc -h
Usage of graphqldoc:
  -endpoint string
    	Endpoint server graphql. Example https://dominio.com/graphql
  -output-dir string
    	Directory with documentation files (default "doc/")
  -without-ext
    	Does not add extension to markdown file links (.md)
$ graphqldoc -endpoint=http://localhost:8080/graphql
```
